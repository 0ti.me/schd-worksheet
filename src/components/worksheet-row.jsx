import React from 'react';

import ensureFloat from '../lib/ensure-float';
import Tooltip from './tooltip';

import './worksheet-row.css';

// TODO: Enable preventing any sort
const WorksheetRow = (props) => {
  const {
    defaultValue,
    irsWorksheetLineNumber,
    label,
    name,
    tooltip,
    onUpdate,
    value,
  } = props;

  const onBlur = (e) => {
    let newValue = ensureFloat(e.target.value);
    newValue = isNaN(newValue) ? 0 : newValue;

    onUpdate(newValue);

    e.target.value = newValue.toFixed(2);

    return newValue;
  };

  const inputProps =
    defaultValue !== undefined
      ? { defaultValue: ensureFloat(defaultValue).toFixed(2), onBlur }
      : { disabled: true, value: ensureFloat(value).toFixed(2) };

  return (
    <div className="row">
      <label className="long-right" htmlFor={name}>
        {label}
      </label>
      <Tooltip tooltip={tooltip} />
      <label htmlFor={name}>{irsWorksheetLineNumber}</label>
      <input {...inputProps} />
    </div>
  );
};

export default WorksheetRow;
