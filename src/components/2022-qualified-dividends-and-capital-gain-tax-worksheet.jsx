import React, { useEffect, useState } from 'react';

import computeTax from '../lib/compute-taxes/2022';
import {
  diffNotLessThanZero,
  maxAndZeroIfNaN,
  minAndZeroIfNaN,
  multiplyAndZeroIfNaN,
  sumNotLessThanZero,
  zeroIfNaN,
} from '../lib/math';
import ensureFloat from '../lib/ensure-float';
import { MARITAL_STATUS } from '../lib/constants';
import MaritalStatusSelection from './marital-status-selection';
import { nextIndexableEntry } from '../lib/random';
import Tooltip from '../components/tooltip';
import upperSpaceCase from '../lib/upper-space-case';
import { useStoredState } from '../lib/state';
import WorksheetRow from './worksheet-row';

import './2022-qualified-dividends-and-capital-gain-tax-worksheet.css';

const zeroPercentThresholdMap = {
  undefined: 0,
  '': 0,
  [MARITAL_STATUS.SINGLE]: '41675.00',
  [MARITAL_STATUS.MARRIED_FILING_JOINTLY]: '83350.00',
  [MARITAL_STATUS.MARRIED_FILING_SEPARATELY]: '41675.00',
  [MARITAL_STATUS.QUALIFYING_WIDOW]: '83350.00',
  [MARITAL_STATUS.HEAD_OF_HOUSEHOLD]: '55800.00',
};

const zeroPercentThresholdMap2 = {
  undefined: 0,
  '': 0,
  [MARITAL_STATUS.SINGLE]: '170050.00',
  [MARITAL_STATUS.MARRIED_FILING_JOINTLY]: '340100.00',
  [MARITAL_STATUS.MARRIED_FILING_SEPARATELY]: '170050.00',
  [MARITAL_STATUS.QUALIFYING_WIDOW]: '340100.00',
  [MARITAL_STATUS.HEAD_OF_HOUSEHOLD]: '170050.00',
};

const fifteenPercentThresholdMap = {
  undefined: 0,
  '': 0,
  [MARITAL_STATUS.SINGLE]: '459750.00',
  [MARITAL_STATUS.MARRIED_FILING_JOINTLY]: '517200.00',
  [MARITAL_STATUS.MARRIED_FILING_SEPARATELY]: '258600.00',
  [MARITAL_STATUS.QUALIFYING_WIDOW]: '517200.00',
  [MARITAL_STATUS.HEAD_OF_HOUSEHOLD]: '488500.00',
};

const wordSalad = [
  [
    'Interactive',
    'Interdependent',
    'Intergalactic',
    'Interesting',
    'Internet',
    'Intentional',
    'International',
    'Interrupting',
    'Intelligent',
    'Interior',
  ],
  ['Revolting', 'Revolutionary', 'Revisionist', 'Reversing', 'Revived'],
  ['Serpents', 'Serenas', 'Sergeis', 'Sergeants', 'Serbians'],
];

const QualifiedDividendsAndCapitalGainTaxWorksheet = () => {
  const [broughtToYouBy, setBroughtToYouBy] = useState('');

  const [maritalStatus, setMaritalStatus] = useStoredState(
    'marital-status',
    '',
  );

  // don't keep updating it
  const defaultMaritalStatus = maritalStatus;

  // Line 1
  const [taxableIncome, setTaxableIncome] = useStoredState(
    'taxaxble-income',
    '',
  );
  // Line 2
  const [qualifiedDividends, setQualifiedDividends] = useStoredState(
    'qualified-dividends',
    '',
  );
  // Line 3
  const [investmentInterestExclusion, setInvestmentInterestExclusion] =
    useStoredState('investment-interest-exclusion', '');
  // Line 4
  const [otherExclusion, setOtherExclusion] = useStoredState(
    'other-exclusion',
    '',
  );
  // Line 5
  const [combinedExclusions, setCombinedExclusions] = useState('');
  // Line 6
  const [
    qualifiedDividendsLessExclusions,
    setQualifiedDividendsLessExclusions,
  ] = useState('');
  // Line 7
  const [scheduleDCapitalGains, setScheduleDCapitalGains] = useStoredState(
    'schedule-d-capital-gains',
    '',
  );
  // Line 8
  const [minimumExclusion, setMinimumExclusion] = useState('');
  // Line 9
  const [gainsLessExclusions, setGainsLessExclusions] = useState('');
  // Line 10
  const [gainsAndDividendsLessExclusions, setGainsAndDividendsLessExclusions] =
    useState('');
  // Line 11
  const [rateGainAndRecaptured, setRateGainAndRecaptured] = useStoredState(
    'rate-gain-and-recaptured',
    '',
  );
  // Line 12
  const [minGainsFromSchdWorksheet, setMinGainsFromSchdWorksheet] =
    useState('');
  // Line 13
  const [
    gainsAndDividendsLessExclusions2,
    setGainsAndDividendsLessExclusions2,
  ] = useState('');
  // Line 14
  const [taxableIncomeLessGains, setTaxableIncomeLessGains] = useState('');
  // Line 15
  const [staticZeroPercentThreshold, setStaticZeroPercentThreshold] =
    useState('');
  // Line 16
  const [lesserOfMaxZeroPercentOrIncome, setLesserOfMaxZeroPercentOrIncome] =
    useState('');
  // Line 17
  const [lesserOfMaxZeroPercentOrIncome2, setLesserOfMaxZeroPercentOrIncome2] =
    useState('');
  // Line 18
  const [
    taxableIncomeLessGainsAndDividendsLessExclusions,
    setTaxableIncomeLessGainsAndDividendsLessExclusions,
  ] = useState('');
  // Line 19
  const [staticZeroPercentThreshold2, setStaticZeroPercentThreshold2] =
    useState('');
  // Line 20
  const [lesserOfLine14And19, setLesserOfLine14And19] = useState('');
  // Line 21
  const [finalTaxableIncomeLessGains, setFinalTaxableIncomeLessGains] =
    useState('');
  // Line 22
  const [quantityTaxedAt0Pct, setQuantityTaxedAt0Pct] = useState('-');
  // Line 23
  const [line23, setLine23] = useState('');
  // Line 24
  const [line24, setLine24] = useState('');
  // Line 25
  const [
    remainingCapitalGainsToTaxMethod1,
    setRemainingCapitalGainsToTaxMethod1,
  ] = useState('');
  // Line 26
  const [maxIncomeRuleFor15PctTaxRate, setMaxIncomeRuleFor15PctTaxRate] =
    useState('');
  // Line 27
  const [maxIncomeFor15PctTaxRate, setMaxIncomeFor15PctTaxRate] = useState('');
  // Line 28
  const [
    incomeLessRemainingTaxableCapitalGains,
    setIncomeLessRemainingTaxableCapitalGains,
  ] = useState('');
  // Line 29
  const [
    remainingCapitalGainsToTaxMethod2,
    setRemainingCapitalGainsToTaxMethod2,
  ] = useState('');
  // Line 30
  const [quantityTaxedAt15Pct, setQuantityTaxedAt15Pct] = useState('');
  // Line 31
  const [taxDueFor15PctPortion, setTaxDueFor15PctPortion] = useState('');
  // Line 32
  const [
    sumOfCapitalGainsTaxedAt0PctAnd15Pct,
    setSumOfCapitalGainsTaxedAt0PctAnd15Pct,
  ] = useState('');
  // Line 33
  const [quantityTaxedAt20Pct, setQuantityTaxedAt20Pct] = useState('');
  // Line 34
  const [taxDueFor20PctPortion, setTaxDueFor20PctPortion] = useState('');
  // Line 35
  const [line35, setLine35] = useState('');
  // Line 36
  const [line36, setLine36] = useState('');
  // Line 37
  const [line37, setLine37] = useState('');
  // Line 38
  const [line38, setLine38] = useState('');
  // Line 39
  const [quantityTaxedAt25Pct, setQuantityTaxedAt25Pct] = useState('');
  // Line 40
  const [taxDueFor25PctPortion, setTaxDueFor25PctPortion] = useState('');
  // Line 41
  const [line41, setLine41] = useState('');
  // Line 42
  const [quantityTaxedAt28Pct, setLine42] = useState('');
  // Line 43
  const [taxDueFor28PctPortion, setTaxDueFor28PctPortion] = useState('');
  // Line 44
  const [
    regularIncomeTaxIfGainsAreTaxedAsGains,
    setRegularIncomeTaxIfGainsAreTaxedAsGains,
  ] = useState('');
  // Line 45
  const [taxIfGainsAreTaxedAsGains, setTaxIfGainsAreTaxedAsGains] =
    useState('');
  // Line 46
  const [taxIfGainsAreTaxedAsIncome, setTaxIfGainsAreTaxedAsIncome] =
    useState('');
  // Line 47
  const [computedTax, setComputedTax] = useState('-');
  // Line 998 (not in worksheet but necessary) - Schedule D Line 18
  const [scheduleDLine18, setScheduleDLine18] = useStoredState(
    'schedule-d-line-18',
    '',
  );
  // Line 999 (not in worksheet but necessary) - Schedule D Line 19
  const [scheduleDLine19, setScheduleDLine19] = useStoredState(
    'schedule-d-line-19',
    '',
  );

  useEffect(() => {
    // Line 5 Calculation
    setCombinedExclusions(
      diffNotLessThanZero(investmentInterestExclusion, otherExclusion),
    );
  }, [investmentInterestExclusion, otherExclusion, setCombinedExclusions]);

  useEffect(() => {
    // Line 6 Calculation
    setQualifiedDividendsLessExclusions(
      diffNotLessThanZero(qualifiedDividends, combinedExclusions),
    );
  }, [
    qualifiedDividends,
    combinedExclusions,
    setQualifiedDividendsLessExclusions,
  ]);

  useEffect(() => {
    // Line 8 Calculation
    setMinimumExclusion(
      minAndZeroIfNaN(investmentInterestExclusion, otherExclusion),
    );
  }, [investmentInterestExclusion, otherExclusion, setMinimumExclusion]);

  useEffect(() => {
    // Line 9 Calculation
    setGainsLessExclusions(
      diffNotLessThanZero(scheduleDCapitalGains, minimumExclusion),
    );
  }, [scheduleDCapitalGains, minimumExclusion, setGainsLessExclusions]);

  useEffect(() => {
    // Line 10 Calculation
    setGainsAndDividendsLessExclusions(
      sumNotLessThanZero(qualifiedDividendsLessExclusions, gainsLessExclusions),
    );
  }, [
    qualifiedDividendsLessExclusions,
    gainsLessExclusions,
    setGainsAndDividendsLessExclusions,
  ]);

  useEffect(() => {
    // Line 12 Calculation
    setMinGainsFromSchdWorksheet(
      minAndZeroIfNaN(gainsLessExclusions, rateGainAndRecaptured),
    );
  }, [
    gainsLessExclusions,
    rateGainAndRecaptured,
    setMinGainsFromSchdWorksheet,
  ]);

  useEffect(() => {
    // Line 13 Calculation
    setGainsAndDividendsLessExclusions2(
      diffNotLessThanZero(
        gainsAndDividendsLessExclusions,
        minGainsFromSchdWorksheet,
      ),
    );
  }, [
    gainsAndDividendsLessExclusions,
    minGainsFromSchdWorksheet,
    setGainsAndDividendsLessExclusions2,
  ]);

  useEffect(() => {
    // Line 14 Calculation
    setTaxableIncomeLessGains(
      diffNotLessThanZero(taxableIncome, gainsAndDividendsLessExclusions2),
    );
  }, [
    taxableIncome,
    gainsAndDividendsLessExclusions2,
    setTaxableIncomeLessGains,
  ]);

  useEffect(() => {
    // Line 15 Calculation
    setStaticZeroPercentThreshold(zeroPercentThresholdMap[maritalStatus]);
  }, [maritalStatus, setStaticZeroPercentThreshold]);

  useEffect(() => {
    // Line 16 Calculation
    setLesserOfMaxZeroPercentOrIncome(
      minAndZeroIfNaN(taxableIncome, staticZeroPercentThreshold),
    );
  }, [
    taxableIncome,
    staticZeroPercentThreshold,
    setLesserOfMaxZeroPercentOrIncome,
  ]);

  useEffect(() => {
    // Line 17 Calculation
    setLesserOfMaxZeroPercentOrIncome2(
      minAndZeroIfNaN(taxableIncomeLessGains, lesserOfMaxZeroPercentOrIncome),
    );
  }, [
    taxableIncomeLessGains,
    lesserOfMaxZeroPercentOrIncome,
    setLesserOfMaxZeroPercentOrIncome2,
  ]);

  useEffect(() => {
    // Line 18 Calculation
    setTaxableIncomeLessGainsAndDividendsLessExclusions(
      diffNotLessThanZero(
        taxableIncomeLessGains,
        gainsAndDividendsLessExclusions,
      ),
    );
  }, [
    taxableIncomeLessGains,
    gainsAndDividendsLessExclusions,
    setTaxableIncomeLessGainsAndDividendsLessExclusions,
  ]);

  useEffect(() => {
    // Line 19 Calculation
    setStaticZeroPercentThreshold2(zeroPercentThresholdMap2[maritalStatus]);
  }, [maritalStatus]);

  useEffect(() => {
    // Line 20 Calculation
    setLesserOfLine14And19(
      minAndZeroIfNaN(taxableIncomeLessGains, staticZeroPercentThreshold2),
    );
  }, [
    taxableIncomeLessGains,
    staticZeroPercentThreshold2,
    setLesserOfLine14And19,
  ]);

  useEffect(() => {
    // Line 21 Calculation
    setFinalTaxableIncomeLessGains(
      maxAndZeroIfNaN(
        taxableIncomeLessGainsAndDividendsLessExclusions,
        lesserOfLine14And19,
      ),
    );
  }, [
    taxableIncomeLessGainsAndDividendsLessExclusions,
    lesserOfLine14And19,
    setFinalTaxableIncomeLessGains,
  ]);

  useEffect(() => {
    // Line 22 Calculation
    setQuantityTaxedAt0Pct(
      diffNotLessThanZero(
        lesserOfMaxZeroPercentOrIncome,
        lesserOfMaxZeroPercentOrIncome2,
      ),
    );
  }, [
    lesserOfMaxZeroPercentOrIncome,
    lesserOfMaxZeroPercentOrIncome2,
    setQuantityTaxedAt0Pct,
  ]);

  useEffect(() => {
    // Line 23 Calculation
    setLine23(minAndZeroIfNaN(taxableIncome, gainsAndDividendsLessExclusions));
  }, [setLine23, taxableIncome, gainsAndDividendsLessExclusions]);

  useEffect(() => {
    // Line 24 Calculation
    setLine24(zeroIfNaN(quantityTaxedAt0Pct));
  }, [quantityTaxedAt0Pct, setLine24]);

  useEffect(() => {
    // Line 25 Calculation
    setRemainingCapitalGainsToTaxMethod1(diffNotLessThanZero(line23, line24));
  }, [line23, line24, setRemainingCapitalGainsToTaxMethod1]);

  useEffect(() => {
    // Line 26 Calculation
    setMaxIncomeRuleFor15PctTaxRate(fifteenPercentThresholdMap[maritalStatus]);
  }, [maritalStatus, setMaxIncomeRuleFor15PctTaxRate]);

  useEffect(() => {
    // Line 27 Calculation
    setMaxIncomeFor15PctTaxRate(
      minAndZeroIfNaN(taxableIncome, maxIncomeRuleFor15PctTaxRate),
    );
  }, [
    taxableIncome,
    maxIncomeRuleFor15PctTaxRate,
    setMaxIncomeRuleFor15PctTaxRate,
  ]);

  useEffect(() => {
    // Line 28 Calculation
    setIncomeLessRemainingTaxableCapitalGains(
      sumNotLessThanZero(finalTaxableIncomeLessGains, quantityTaxedAt0Pct),
    );
  }, [
    finalTaxableIncomeLessGains,
    quantityTaxedAt0Pct,
    setIncomeLessRemainingTaxableCapitalGains,
  ]);

  useEffect(() => {
    // Line 29 Calculation
    setRemainingCapitalGainsToTaxMethod2(
      diffNotLessThanZero(
        maxIncomeFor15PctTaxRate,
        incomeLessRemainingTaxableCapitalGains,
      ),
    );
  }, [
    maxIncomeFor15PctTaxRate,
    incomeLessRemainingTaxableCapitalGains,
    setRemainingCapitalGainsToTaxMethod2,
  ]);

  useEffect(() => {
    // Line 30 Calculation
    setQuantityTaxedAt15Pct(
      minAndZeroIfNaN(
        remainingCapitalGainsToTaxMethod1,
        remainingCapitalGainsToTaxMethod2,
      ),
    );
  }, [
    remainingCapitalGainsToTaxMethod1,
    remainingCapitalGainsToTaxMethod2,
    setQuantityTaxedAt15Pct,
  ]);

  useEffect(() => {
    // Line 31 Calculation
    setTaxDueFor15PctPortion(multiplyAndZeroIfNaN(quantityTaxedAt15Pct, 0.15));
  }, [quantityTaxedAt15Pct, setTaxDueFor15PctPortion]);

  useEffect(() => {
    // Line 32 Calculation
    setSumOfCapitalGainsTaxedAt0PctAnd15Pct(
      sumNotLessThanZero(line24, quantityTaxedAt15Pct),
    );
  }, [line24, quantityTaxedAt15Pct, setSumOfCapitalGainsTaxedAt0PctAnd15Pct]);

  useEffect(() => {
    // Line 33 Calculation
    setQuantityTaxedAt20Pct(
      diffNotLessThanZero(line23, sumOfCapitalGainsTaxedAt0PctAnd15Pct),
    );
  }, [line23, sumOfCapitalGainsTaxedAt0PctAnd15Pct, setQuantityTaxedAt20Pct]);

  useEffect(() => {
    // Line 34 Calculation
    setTaxDueFor20PctPortion(multiplyAndZeroIfNaN(quantityTaxedAt20Pct, 0.2));
  }, [quantityTaxedAt20Pct, setTaxDueFor20PctPortion]);

  useEffect(() => {
    // Line 35 Calculation
    setLine35(minAndZeroIfNaN(gainsLessExclusions, scheduleDLine19));
  }, [gainsLessExclusions, scheduleDLine19, setLine35]);

  useEffect(() => {
    // Line 36 Calculation
    setLine36(
      sumNotLessThanZero(
        gainsAndDividendsLessExclusions,
        finalTaxableIncomeLessGains,
      ),
    );
  }, [gainsAndDividendsLessExclusions, finalTaxableIncomeLessGains, setLine36]);

  useEffect(() => {
    // Line 37 Calculation
    setLine37(taxableIncome);
  }, [taxableIncome, setLine37]);

  useEffect(() => {
    // Line 38 Calculation
    setLine38(diffNotLessThanZero(line36, line37));
  }, [line36, line37, setLine38]);

  useEffect(() => {
    // Line 39 Calculation
    setQuantityTaxedAt25Pct(diffNotLessThanZero(line35, line38));
  }, [line35, line38, setQuantityTaxedAt25Pct]);

  useEffect(() => {
    // Line 40 Calculation
    setTaxDueFor25PctPortion(multiplyAndZeroIfNaN(quantityTaxedAt25Pct, 0.25));
  }, [quantityTaxedAt25Pct, setTaxDueFor25PctPortion]);

  useEffect(() => {
    // Line 41 Calculation
    setLine41(
      sumNotLessThanZero(
        finalTaxableIncomeLessGains,
        quantityTaxedAt0Pct,
        quantityTaxedAt15Pct,
        quantityTaxedAt20Pct,
        quantityTaxedAt25Pct,
      ),
    );
  }, [
    finalTaxableIncomeLessGains,
    quantityTaxedAt0Pct,
    quantityTaxedAt15Pct,
    quantityTaxedAt20Pct,
    quantityTaxedAt25Pct,
    setLine41,
  ]);

  useEffect(() => {
    // Line 42 Calculation
    setLine42(scheduleDLine18 ? diffNotLessThanZero(taxableIncome, line41) : 0);
  }, [scheduleDLine18, taxableIncome, line41, setLine42]);

  useEffect(() => {
    // Line 43 Calculation
    setTaxDueFor28PctPortion(
      scheduleDLine18 ? multiplyAndZeroIfNaN(quantityTaxedAt28Pct, 0.28) : 0,
    );
  }, [scheduleDLine18, quantityTaxedAt28Pct, setTaxDueFor28PctPortion]);

  useEffect(() => {
    // Line 44 Calculation
    setRegularIncomeTaxIfGainsAreTaxedAsGains(
      maritalStatus
        ? computeTax(finalTaxableIncomeLessGains, maritalStatus)
        : 0,
    );
  }, [
    finalTaxableIncomeLessGains,
    maritalStatus,
    setRegularIncomeTaxIfGainsAreTaxedAsGains,
  ]);

  useEffect(() => {
    // Line 45 Calculation
    setTaxIfGainsAreTaxedAsGains(
      sumNotLessThanZero(
        taxDueFor15PctPortion,
        taxDueFor20PctPortion,
        taxDueFor25PctPortion,
        taxDueFor28PctPortion,
        regularIncomeTaxIfGainsAreTaxedAsGains,
      ),
    );
  }, [
    taxDueFor15PctPortion,
    taxDueFor20PctPortion,
    taxDueFor25PctPortion,
    taxDueFor28PctPortion,
    regularIncomeTaxIfGainsAreTaxedAsGains,
    setTaxIfGainsAreTaxedAsGains,
  ]);

  useEffect(() => {
    // Line 46 Calculation
    setTaxIfGainsAreTaxedAsIncome(
      maritalStatus ? computeTax(taxableIncome, maritalStatus) : 0,
    );
  }, [taxableIncome, maritalStatus, setTaxIfGainsAreTaxedAsIncome]);

  useEffect(() => {
    // Line 47 Calculation
    setComputedTax(
      minAndZeroIfNaN(taxIfGainsAreTaxedAsGains, taxIfGainsAreTaxedAsIncome),
    );
  }, [taxIfGainsAreTaxedAsGains, taxIfGainsAreTaxedAsIncome, setComputedTax]);

  const rows = [
    {
      // Line 1
      defaultValue: taxableIncome,
      label: 'Taxable Income 140 Line 15',
      name: 'taxable-income',
      onUpdate: setTaxableIncome,
      tooltip: 'You can find this on your 1040 on line number 15',
    },
    {
      // Line 2
      defaultValue: qualifiedDividends,
      label: 'Qualified Dividends 1040 Line 3a',
      name: 'qualified-dividends',
      onUpdate: setQualifiedDividends,
      tooltip: 'You can find this on your 1040 on line number 3a',
    },
    {
      // Line 3
      defaultValue: investmentInterestExclusion,
      label: 'Investment Interest 4952 Line 4g',
      name: 'investment-interest-exclusion',
      onUpdate: setInvestmentInterestExclusion,
      tooltip: 'You can find this on your 4952 Line 4g',
    },
    {
      // Line 4
      defaultValue: otherExclusion,
      label: 'Other Exclusion 4952 Line 4e',
      name: 'other-exclusion',
      onUpdate: setOtherExclusion,
      tooltip: 'You can find this on your 4952 Line 4e',
    },
    {
      // Line 5
      label: 'Combined Exclusions from 4952',
      name: '4952-combined-exclusions',
      tooltip: 'Calculated be line 3 - line 4',
      value: combinedExclusions,
    },
    {
      // Line 6
      name: 'qualified-dividends-less-exclusions',
      tooltip: 'Calculated be line 2 - line 5',
      value: qualifiedDividendsLessExclusions,
    },
    {
      // Line 7
      defaultValue: scheduleDCapitalGains,
      label: 'Total Capital Gains from 1040 Schedule D',
      name: 'schedule-d-capital-gains',
      onUpdate: setScheduleDCapitalGains,
      tooltip: 'Smaller of Line 15 or Line 16 from 1040 Schedule D',
    },
    {
      // Line 8
      name: 'minimum-exlusion',
      value: minimumExclusion,
    },
    {
      // Line 9
      name: 'gains-less-exlusions',
      value: gainsLessExclusions,
    },
    {
      // Line 10
      name: 'gains-and-dividends-less-exclusions',
      value: gainsAndDividendsLessExclusions,
    },
    {
      // Line 11
      defaultValue: rateGainAndRecaptured,
      label: 'Rate Gain & Recaptured from 1040 Schedule D',
      name: 'rate-gain-and-recaptured',
      onUpdate: setRateGainAndRecaptured,
      tooltip: 'Sum of Line 18 and Line 19 from Schedule D',
    },
    {
      // Line 12
      name: 'min-gains-from-schedule-d-worksheet',
      value: minGainsFromSchdWorksheet,
    },
    {
      // Line 13
      name: 'gains-and-dividends-less-exclusions-2',
      value: gainsAndDividendsLessExclusions2,
    },
    {
      // Line 14
      name: 'taxable-income-less-gains',
      value: taxableIncomeLessGains,
    },
    {
      // Line 15
      name: 'static-zero-percent-tax-threshold',
      value: staticZeroPercentThreshold,
    },
    {
      // Line 16
      label: 'Lesser of Max Zero Percent Threshold Value or Taxable Income',
      name: 'lesser-of-max-zero-percent-or-income',
      value: lesserOfMaxZeroPercentOrIncome,
    },
    {
      // Line 17
      label:
        'Lesser of Max Zero Percent Threshold Value or Taxable Income (round 2?)',
      name: 'lesser-of-max-zero-percent-or-income2',
      value: lesserOfMaxZeroPercentOrIncome2,
    },
    // Line 18
    {
      name: 'taxable-income-less-gains-and-dividends-less-exclusions',
      value: taxableIncomeLessGainsAndDividendsLessExclusions,
    },
    {
      // Line 19
      name: 'static-2',
      value: staticZeroPercentThreshold2,
    },
    {
      // Line 20
      name: 'lesser-of-14-and-19',
      value: lesserOfLine14And19,
    },
    {
      // Line 21
      name: 'final-taxable-income-less-gains',
      value: finalTaxableIncomeLessGains,
    },
    {
      // Line 22
      label: 'How much less than the max (for 0% rate) income was made',
      name: 'quantity-less-than-max-for-0%-rate',
      tooltip: 'This is the amount of your gains you pay 0% on',
      value: quantityTaxedAt0Pct,
    },
    {
      // Line 23
      name: 'lesser-of-line-1-and-line-13',
      value: line23,
    },
    {
      // Line 24
      name: 'line-24',
      value: line24,
    },
    {
      // Line 25
      name: 'remaining-capital-gains-to-tax-method-1',
      value: remainingCapitalGainsToTaxMethod1,
    },
    {
      // Line 26
      name: 'max-income-rule-for-15%-tax-rate',
      value: maxIncomeRuleFor15PctTaxRate,
    },
    {
      // Line 27
      name: 'max-income-for-15%-tax-rate',
      value: maxIncomeFor15PctTaxRate,
    },
    {
      // Line 28
      name: 'income-less-remaining-taxable-capital-gains',
      value: incomeLessRemainingTaxableCapitalGains,
    },
    {
      // Line 29
      name: 'remaining-capital-gains-to-tax-method-2',
      value: remainingCapitalGainsToTaxMethod2,
    },
    {
      // Line 30
      name: 'remaining-capital-gains-to-tax-at-15%',
      tooltip: 'This is the amount of your gains you pay 15% on',
      value: quantityTaxedAt15Pct,
    },
    {
      // Line 31
      name: 'tax-due-for-15%-portion',
      tooltip:
        'This is the total amount due for that portion of your gains at 15%',
      value: taxDueFor15PctPortion,
    },
    {
      // Line 32
      name: 'sum-of-capital-gains-taxed-at-0%-and-15%',
      value: sumOfCapitalGainsTaxedAt0PctAnd15Pct,
    },
    {
      // Line 33
      name: 'cap-gains-remaining-to-be-paid',
      tooltip: 'This is the amount of your gains you pay 20% on',
      value: quantityTaxedAt20Pct,
    },
    {
      // Line 34
      name: 'tax-due-for-20%-portion',
      tooltip:
        'This is the total amount due for that portion of your gains at 20%',
      value: taxDueFor20PctPortion,
    },
    {
      // Line 35
      name: 'line-35',
      value: line35,
    },
    {
      // Line 36
      name: 'line-36',
      value: line36,
    },
    {
      // Line 37
      name: 'line-37',
      value: line37,
    },
    {
      // Line 38
      name: 'line-38',
      value: line38,
    },
    {
      // Line 39
      name: 'line-39',
      value: quantityTaxedAt25Pct,
    },
    {
      // Line 40
      name: 'line-40',
      value: taxDueFor25PctPortion,
    },
    {
      // Line 41
      name: 'quantity-taxed-so-far',
      value: line41,
    },
    {
      // Line 42
      name: 'net-investment-income-rate-taxable-amount',
      tooltip: 'I think this is the net investment taxable total',
      value: quantityTaxedAt28Pct,
    },
    {
      // Line 43
      name: 'net-investment-tax-total',
      tooltip: 'I think this is the net investment tax amount due',
      value: taxDueFor28PctPortion,
    },
    {
      // Line 44
      name: 'tax-if-gains-are-taxed-as-gains',
      value: regularIncomeTaxIfGainsAreTaxedAsGains,
    },
    {
      // Line 45
      name: 'total-tax-with-gains-as-gains',
      tooltip:
        'This is the total tax if gains are treated as gains instead of income (usually better)',
      value: taxIfGainsAreTaxedAsGains,
    },
    {
      // Line 46
      name: 'tax-if-gains-are-taxed-as-income',
      tooltip: 'This is the total tax if gains are treated as income',
      value: taxIfGainsAreTaxedAsIncome,
    },
    {
      // Line 47
      name: 'tax-on-all-taxable-income',
      tooltip:
        'This is the in-no-way-recommended amount to put on 1040 line 16',
      value: computedTax,
    },
    {
      // Line 998 (not in worksheet but necessary) - Schedule D Line 19
      defaultValue: scheduleDLine18,
      name: 'schedule-d-line-18',
      onUpdate: setScheduleDLine18,
      tooltip:
        'Not specified on the original worksheet, this is a field still needed to complete the worksheet',
    },
    {
      // Line 999 (not in worksheet but necessary) - Schedule D Line 18
      defaultValue: scheduleDLine19,
      name: 'schedule-d-line-19',
      onUpdate: setScheduleDLine19,
      tooltip:
        'Not specified on the original worksheet, this is a field still needed to complete the worksheet',
    },
  ]
    .map((row, i) => ({
      label: upperSpaceCase(row.name),
      ...row,
      irsWorksheetLineNumber: i + 1,
      key: `${i}-irs-worksheet-row`,
    }))
    .sort((a, b) => {
      if (
        (a.defaultValue === undefined && b.defaultValue === undefined) ||
        (a.defaultValue !== undefined && b.defaultValue !== undefined)
      ) {
        // priotize earlier line numbers first if presence of value is the same
        return a.irsWorksheetLineNumber > b.irsWorksheetLineNumber ? 1 : -1;
      } else {
        // prioritize absent args over present args
        return a.defaultValue === undefined ? 1 : -1;
      }
    })
    .map((props) => <WorksheetRow {...props} />);

  const summary = [
    {
      key: 'computed-tax-total',
      tooltip:
        'This is the in-no-way-recommended amount to put on 1040 line 16',
      value: computedTax,
    },
    {
      key: 'capital-gains-portion-taxed-at-0%',
      tooltip: 'This is the amount of your gains you pay 0% on',
      value: quantityTaxedAt0Pct,
    },
    {
      key: 'tax-total-for-0%-portion-(always-0)',
      tooltip: 'Turns out 0% of any amount is still 0, who would have thought?',
      value: 0,
    },
    {
      key: 'capital-gains-portion-taxed-at-15%',
      tooltip: 'This is the amount of your gains you pay 15% on',
      value: quantityTaxedAt15Pct,
    },
    {
      key: 'tax-total-for-15%-portion',
      tooltip:
        'This is the total amount due for that portion of your gains at 15%',
      value: taxDueFor15PctPortion,
    },
    {
      key: 'capital-gains-portion-taxed-at-20%',
      tooltip: 'This is the amount of your gains you pay 20% on',
      value: quantityTaxedAt20Pct,
    },
    {
      key: 'tax-total-for-20%-portion',
      tooltip:
        'This is the total amount due for that portion of your gains at 20%',
      value: taxDueFor20PctPortion,
    },
    {
      key: '25%-portion',
      tooltip:
        'I think this is for gains on certain types of collectibles like gold, this would be the income in that category to be taxed at 25%',
      value: quantityTaxedAt25Pct,
    },
    {
      key: 'tax-total-for-25%-portion',
      tooltip:
        'I think this is for gains on certain types of collectibles like gold, this would be the tax due for that category of gains',
      value: taxDueFor25PctPortion,
    },
    {
      key: 'capital-gains-portion-taxed-at-28%',
      tooltip: 'I think this is the net investment taxable total',
      value: quantityTaxedAt28Pct,
    },
    {
      key: 'tax-total-for-28%-portion',
      tooltip: 'I think this is the net investment tax amount due',
      value: taxDueFor28PctPortion,
    },
  ].map(({ key, tooltip, value }) => (
    <div className="row" key={key}>
      <div className="long-right">{upperSpaceCase(key)}</div>
      <Tooltip tooltip={tooltip} />
      <div className="long-left">
        ${value === '-' ? value : ensureFloat(value).toFixed(2)}
      </div>
    </div>
  ));

  useEffect(() => {
    setBroughtToYouBy(wordSalad.map(nextIndexableEntry).join(' '));
  }, [
    setBroughtToYouBy,
    taxableIncome,
    qualifiedDividends,
    investmentInterestExclusion,
    otherExclusion,
    scheduleDCapitalGains,
    rateGainAndRecaptured,
    scheduleDLine18,
    scheduleDLine19,
  ]);

  return (
    <>
      <h2>Qualified Dividends and Capital Gain Tax Worksheet</h2>
      <h3>Brought to you by the {broughtToYouBy}</h3>
      <div className="flex-container">
        <div>
          <h3>Worksheet</h3>
          <h4 className="constrained-paragraph warn-red">
            Please do not use this without double checking the math yourself, I
            am not an accountant, I am your computer, and none of this is tax
            advice.
          </h4>
          <h4 className="constrained-paragraph warn-red">
            I made this because of a bug in my formulas in a spreadsheet, and I
            could not find the problem. So I figured I'd make something that
            other people might be able to use for similar purposes.
          </h4>
          <h4 className="constrained-paragraph warn-red">
            It's buggy so refresh the page if you have problems. The program
            intends to retain your data. Good luck.
          </h4>
          <h4 className="constrained-paragraph warn-red">
            I can't believe I had to make this because the IRS didn't bother
            making a good one.
          </h4>
        </div>
        <div className="inner-flex-container">{summary}</div>
        <MaritalStatusSelection
          defaultValue={defaultMaritalStatus}
          name="marital-status-selection"
          onUpdate={setMaritalStatus}
        />
        <p />
        <h3>The Rest of the Form</h3>
        <h4 className="constrained-paragraph">
          Ignore the many, many fields which are grayed out except for checking
          your math, they are calculated values.
        </h4>
        <p />
        {rows}
      </div>
    </>
  );
};

export default QualifiedDividendsAndCapitalGainTaxWorksheet;
