import React, { useState } from 'react';

import collate from '../lib/collate';
import logger from '../lib/logger';
import { MARITAL_STATUS } from '../lib/constants';
import Tooltip from './tooltip';
import upperSpaceCase from '../lib/upper-space-case';

const maritalValues = [
  MARITAL_STATUS.SINGLE,
  MARITAL_STATUS.MARRIED_FILING_SEPARATELY,
  MARITAL_STATUS.MARRIED_FILING_JOINTLY,
  MARITAL_STATUS.HEAD_OF_HOUSEHOLD,
  MARITAL_STATUS.QUALIFYING_WIDOW,
];

const MaritalStatusSelection = (props) => {
  const { defaultValue, name, onUpdate } = props;

  const [maritalStatus, setMaritalStatus] = useState(defaultValue);

  const maritalRadioData = maritalValues.map((value) => ({
    key: `marital-status-${value}`,
    label: upperSpaceCase(value),
    value,
  }));

  const maritalRadios = maritalRadioData.map(({ key, value }) => {
    const inputProps = {
      id: key,
      key,
      name: 'marital-status',
      onChange: (e) => {
        const value = e.target.value;

        logger.debug(
          `${name}: notifying of marital status value change in onChange`,
        );

        onUpdate(value);
        setMaritalStatus(value);
      },
      type: 'radio',
      value,
    };

    if (maritalStatus === value) {
      inputProps.defaultChecked = true;
    }

    return <input {...inputProps} />;
  });

  const maritalRadioLabels = maritalRadioData.map(({ key, label }) => (
    <label key={`label-for-${key}`} htmlFor={key}>
      {label}
    </label>
  ));

  const eles = collate(maritalRadios, maritalRadioLabels);

  return (
    <div>
      <h3>
        Marital Status selection{' '}
        <Tooltip tooltip="Marital status is required to calculate several aspects of your tax rate" />
      </h3>
      {eles}
    </div>
  );
};

export default MaritalStatusSelection;
