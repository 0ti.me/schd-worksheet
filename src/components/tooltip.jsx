import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import MatTooltip from '@mui/material/Tooltip';
import Zoom from '@mui/material/Zoom';

import './tooltip.css';

const DEFAULT_TOOLTIP_TEXT = 'Your guess is as good as mine';

const Tooltip = ({ icon, tooltip }) => {
  return (
    <MatTooltip
      arrow
      classes={{ tooltip: 'tooltip-large' }}
      disableInteractive={true}
      placement="top"
      title={tooltip || DEFAULT_TOOLTIP_TEXT}
      TransitionComponent={Zoom}
    >
      <FontAwesomeIcon icon={icon || faQuestionCircle} />
    </MatTooltip>
  );
};

export default Tooltip;
