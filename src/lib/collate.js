const collate = (...lists) => {
  let length = lists[0].length;
  const collated = new Array(length * lists.length);
  let cIndx = 0;

  for (let j = 0; j < length; ++j) {
    for (let i = 0; i < lists.length; ++i) {
      if (lists[i].length !== length) {
        throw new Error('all lists must be the same length');
      }

      collated[cIndx++] = lists[i][j];
    }
  }

  return collated;
};

export default collate;
