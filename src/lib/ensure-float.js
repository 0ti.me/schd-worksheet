let textDecoder;
let textEncoder;

const ensureFloat = (x) => {
  if (typeof x === 'number') {
    return x;
  }

  if (!textDecoder) {
    textDecoder = new TextDecoder();
    textEncoder = new TextEncoder();
  }

  const numbers = Uint8Array.from(textEncoder.encode(x));
  let storeIndex = 0;

  let decimals = 0;

  for (let i = 0; i < numbers.length; ++i) {
    // only include 0-9 or .
    if ((numbers[i] >= 0x30 && numbers[i] <= 0x39) || numbers[i] === 0x2e) {
      if (numbers[i] === 0x2e) {
        if (decimals >= 1) {
          return NaN;
        }

        ++decimals;
      }
      if (storeIndex !== i) {
        numbers[storeIndex] = numbers[i];
      }

      ++storeIndex;
    }
  }

  for (; storeIndex < numbers.length; ++storeIndex) {
    numbers[storeIndex] = 0;
  }

  // may still be NaN with more than 1 decimal point.
  const value = parseFloat(textDecoder.decode(numbers));

  return isNaN(value) ? 0 : value;
};

export default ensureFloat;
