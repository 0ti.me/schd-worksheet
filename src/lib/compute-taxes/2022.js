import { MARITAL_STATUS } from '../../lib/constants';

// single/unmarried
const bracketA = [
  { max: 10275, rate: 0.1, subtraction: 0 },
  { max: 41775, rate: 0.12, subtraction: 205.5 },
  { max: 89075, rate: 0.22, subtraction: 4383 },
  { max: 170050, rate: 0.24, subtraction: 6164.5 },
  { max: 215950, rate: 0.32, subtraction: 19768.5 },
  { max: 539900, rate: 0.35, subtraction: 26247.0 },
  { max: Number.MAX_VALUE, rate: 0.37, subtraction: 37045 },
];

// married filing jointly or qualifying widow(er)
const bracketB = [
  { max: 20550, rate: 0.1, subtraction: 0 },
  { max: 83550, rate: 0.12, subtraction: 411 },
  { max: 178150, rate: 0.22, subtraction: 8766 },
  { max: 340100, rate: 0.24, subtraction: 12329 },
  { max: 431900, rate: 0.32, subtraction: 39537 },
  { max: 647850, rate: 0.35, subtraction: 52494 },
  { max: Number.MAX_VALUE, rate: 0.37, subtraction: 65451 },
];

// married filing separately
const bracketC = [
  { max: 10275, rate: 0.1, subtraction: 0 },
  { max: 41775, rate: 0.12, subtraction: 205.5 },
  { max: 89075, rate: 0.22, subtraction: 4383 },
  { max: 170050, rate: 0.24, subtraction: 6164.5 },
  { max: 215950, rate: 0.32, subtraction: 19768.5 },
  { max: 323925, rate: 0.35, subtraction: 26247 },
  { max: Number.MAX_VALUE, rate: 0.37, subtraction: 32725.5 },
];

// head of household
const bracketD = [
  { max: 14650, rate: 0.1, subtraction: 0 },
  { max: 55900, rate: 0.12, subtraction: 293 },
  { max: 89050, rate: 0.22, subtraction: 5883 },
  { max: 170050, rate: 0.24, subtraction: 7664 },
  { max: 215950, rate: 0.32, subtraction: 21268 },
  { max: 539900, rate: 0.35, subtraction: 27746.5 },
  { max: Number.MAX_VALUE, rate: 0.37, subtraction: 38544.5 },
];

const computeTax = (income, maritalStatus) => {
  let brackets;

  if (income <= 0) return '';

  switch (maritalStatus) {
    case MARITAL_STATUS.SINGLE:
      brackets = bracketA;

      break;
    case MARITAL_STATUS.MARRIED_FILING_JOINTLY:
    case MARITAL_STATUS.QUALIFYING_WIDOW:
      brackets = bracketB;

      break;
    case MARITAL_STATUS.MARRIED_FILING_SEPARATELY:
      brackets = bracketC;

      break;
    case MARITAL_STATUS.HEAD_OF_HOUSEHOLD:
      brackets = bracketD;

      break;
    default:
      return '';
  }

  const bracket = brackets.find(({ max }) => max >= income);

  return -bracket.subtraction + income * bracket.rate;
};

export default computeTax;
