export const MARITAL_STATUS = {
  SINGLE: 'single',
  MARRIED_FILING_SEPARATELY: 'married-filing-separately',
  MARRIED_FILING_JOINTLY: 'married-filing-jointly',
  HEAD_OF_HOUSEHOLD: 'head-of-household',
  QUALIFYING_WIDOW: 'qualifying-widow(er)',
};
