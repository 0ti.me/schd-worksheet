import { useState } from 'react';

import { logger } from './logger';

export const getStoredState = (key, defaultValue, options) => {
  if (key === undefined) {
    throw new Error('key cannot be undefined');
  }

  const value = window.localStorage.getItem(key);

  const valueToReturn = value !== null ? value : defaultValue;

  if (options?.onRawGet) {
    options?.onRawGet(key, defaultValue, value, valueToReturn);
  }

  return valueToReturn;
};

export const useStoredState = (key, defaultValue, options) => {
  if (!key) {
    logger.error('key cannot be falsy');
  } else if (defaultValue === undefined) {
    logger.error(`[${key}] defaultValue cannot be undefined`);
  }

  const [value, stateSetter] = useState(
    getStoredState(key, defaultValue, options),
  );

  return [
    value,
    (newValue) => {
      window.localStorage.setItem(key, newValue);

      if (options?.onChange) {
        options?.onChange(key, value, newValue);
      }

      return stateSetter(newValue);
    },
  ];
};
