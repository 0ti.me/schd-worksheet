export const nextInt = (rangeEnd = Number.MAX_SAFE_INTEGER, rangeStart = 0) =>
  Math.floor(Math.random() * (rangeEnd - rangeStart) + rangeStart);

export const nextIndexableEntry = (indexable) =>
  indexable[nextInt(indexable.length)];
