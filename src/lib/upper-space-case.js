let textDecoder;
let textEncoder;

const isLowercaseLetter = (x) => x >= 0x61 && x <= 0x7a;
const isNumber = (x) => x >= 0x30 && x <= 0x37;
const isUppercaseLetter = (x) => x >= 0x41 && x <= 0x5a;
const isAlphanumeric = (x) =>
  isLowercaseLetter(x) || isNumber(x) || isUppercaseLetter(x);

/* given kebab-case, derive the rest, deliberately a naive implementation */
const upperSpaceCase = (kebabInput) => {
  if (!textDecoder) {
    textDecoder = new TextDecoder();
    textEncoder = new TextEncoder();
  }

  const upperSpaceCase = Uint8Array.from(textEncoder.encode(kebabInput));
  const kebab = Uint8Array.from(upperSpaceCase);

  const space = ' '.charCodeAt(0);

  for (let i = 0; i < kebab.length; ++i) {
    if (i > 0 && !isAlphanumeric(kebab[i - 1]) && isLowercaseLetter(kebab[i])) {
      upperSpaceCase[i] = kebab[i] - 32;
    }

    if (kebab[i] === 0x2d) {
      upperSpaceCase[i] = space;
    }
  }

  if (isLowercaseLetter(upperSpaceCase[0])) {
    upperSpaceCase[0] -= 32;
  }

  return textDecoder.decode(upperSpaceCase);
};

export default upperSpaceCase;
