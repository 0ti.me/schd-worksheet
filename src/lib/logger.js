/* eslint-disable no-console */

// eslint-disable-next-line @typescript-eslint/no-empty-function
let debug = () => {};

if (process.env === 'development') {
  debug = (...args) => console.debug('D', ...args);
}

export const logger = {
  debug,
  error: (...args) => console.error('E', ...args),
  info: (...args) => console.info('I', ...args),
  log: (...args) => console.log('L', ...args),
  warn: (...args) => console.warn('W', ...args),
};

export default logger;
