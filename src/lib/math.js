export const zeroIfNaN = (x) => (isNaN(x) ? 0 : x);

export const maxAndZeroIfNaN = (...args) =>
  zeroIfNaN(Math.max(...args.filter((x) => !isNaN(x))));

export const minAndZeroIfNaN = (...args) =>
  zeroIfNaN(Math.min(...args.filter((x) => !isNaN(x))));

export const diffNotLessThanZero = (...args) =>
  maxAndZeroIfNaN(
    args.reduce((acc, ea) => acc - ea),
    0,
  );

export const sumNotLessThanZero = (...args) =>
  maxAndZeroIfNaN(
    args.reduce((acc, ea) => acc + ea),
    0,
  );

export const multiplyAndZeroIfNaN = (...args) =>
  zeroIfNaN(args.reduce((acc, ea) => acc * ea));
