import React from 'react';

import QualifiedDividendsAndCapitalGainTaxWorksheet from './components/2022-qualified-dividends-and-capital-gain-tax-worksheet';

import './App.css';

const App = () => {
  return (
    <div className="App">
      <QualifiedDividendsAndCapitalGainTaxWorksheet />
    </div>
  );
};

export default App;
