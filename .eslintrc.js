const eslintrc = require('@0ti.me/ts-test-deps/configuration-templates/eslintrc');

eslintrc.extends.push('react-app');
eslintrc.overrides.push({
  files: ['**/*.js', '**/*.jsx'],
  rules: {
    'no-unused-vars': 'off',
  },
});

module.exports = eslintrc;
